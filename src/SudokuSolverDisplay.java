import processing.core.PApplet;

public class SudokuSolverDisplay extends PApplet{
	
	int WIN_WIDTH = 1000, WIN_HEIGHT = 1000;
	float bgR =  random(100f, 200f), bgG =  random(100f, 200f), bgB = random(100f, 200f);
	boolean changingColors = false;
	boolean autoSolve = false;
	String file = "puzzle.txt";
	SudokuSolver unsolved = new SudokuSolver(file);
	SudokuSolver solved = new SudokuSolver(file);
	
    public static void main(String[] args) {
    	PApplet.main("SudokuSolverDisplay");
    }
    
    public void settings()
    {
    	size(WIN_WIDTH, WIN_HEIGHT);
    }
    
    public void setup()
    {
    	frameRate(4);
    	background(bgR, bgG, bgB);
    	if(autoSolve)
    	{
    		drawNumbers();
    	}
    	drawSolvedNumbers();
    	
    	drawStyledLines();
    	solved.showPuzzle();
    }
    
    public void draw()
    {
    	if(changingColors){
    		changeColors();
	    	background(bgR, bgG, bgB);if(autoSolve)
	    	{
	    		drawNumbers();
	    	}
	    	drawSolvedNumbers();
	    	
	    	drawStyledLines();
    	}
    }
    
    /**
     *  Draw every solved number
     */
    public void drawSolvedNumbers()
    {
    	for(int r = 0; r < unsolved.getNumbers().length; r++)
    	{
    		for(int c = 0; c < unsolved.getNumbers()[r].length; c++)
    		{
    	    	if(unsolved.getNumbers()[c][r] != 0)
    			{
    	    		fillBox(r,c);
    	    		
    	    		pushStyle();
    	    		
        	    	textSize(64);
        	    	fill(bgG-50, bgB-50, bgR-50);
        	    	
    	    		text(unsolved.getNumbers()[c][r], WIN_WIDTH/9*r+WIN_WIDTH/9/2-18, WIN_HEIGHT/9*c+WIN_HEIGHT/9/2+24);
    	    		
    	    		popStyle();
    			}
    		}
    	}
    }
    
    public void drawNumber(int c, int r)
    {
    	text(unsolved.getNumbers()[c][r], WIN_WIDTH/9*r+WIN_WIDTH/9/2-18, WIN_HEIGHT/9*c+WIN_HEIGHT/9/2+24);
    }
    
    /**
     * Draw rect over box at given position of row and col
     * @param row
     * @param col
     */
    public void fillBox(int row, int col)
    {
    	// Solved fill
		pushStyle();
		
		stroke(bgR-50, bgG-50, bgB-50);
		fill(bgG, bgB, bgR);
		strokeWeight(2);
		
		rect(WIN_WIDTH/9*row, WIN_HEIGHT/9*col, WIN_WIDTH/9, WIN_HEIGHT/9);
		
		popStyle();
    }
    
    /**
     *  Draw every number
     */
    public void drawNumbers()
    {
    	for(int r = 0; r < solved.getNumbers().length; r++)
    	{
    		for(int c = 0; c < solved.getNumbers()[r].length; c++)
    		{
    			solved.sudokuSolver();
    			pushStyle();
    			
    	    	textSize(64);
    	    	fill(bgR-50, bgG-50, bgB-50);
    	    	
	    		text(solved.getNumbers()[c][r], WIN_WIDTH/9*r+WIN_WIDTH/9/2-18, WIN_HEIGHT/9*c+WIN_HEIGHT/9/2+24);
	    		
	    		popStyle();
    		}
    	}
    }
    
    /**
     *  Give lines style
     */
    public void drawStyledLines()
    {
    	decorateThinLines();
    	decorateThickLines();
    }
    
    public void decorateThinLines()
    {
    	pushStyle();
    	
    	stroke(bgR-50, bgG-50, bgB-50);
    	strokeWeight(2);
    	
    	drawLines(9);
    	
    	popStyle();
    }
    
    public void decorateThickLines()
    {
    	pushStyle();
    	
    	stroke(bgG-60, bgB-60, bgR-60);
    	strokeWeight(5);
    	
    	drawLines(3);
    	
    	popStyle();
    }
    
    /**
     * Draw lines every (window size / n)
     * @param n
     */
    public void drawLines(int n)
    {
    	for(int i = 0; i < n; i++)
    	{
    		line(WIN_WIDTH/n*(i+1), 0, WIN_WIDTH/n*(i+1), WIN_HEIGHT);
    	}
    	for(int i = 0; i < n; i++)
    	{
    		line(0, WIN_HEIGHT/n*(i+1), WIN_WIDTH, WIN_HEIGHT/n*(i+1));
    	}
    }
    
    public void keyPressed()
    {
    	if(key == '1')
    	{
    		setPuzzle("puzzle.txt");
    	}
    	else if(key == '2')
    	{
    		setPuzzle("puzzle1.txt");
    	}
    	else if(key == '3')
    	{
    		setPuzzle("puzzle2.txt");
    	}
    	else if(key == 'c' && !changingColors)
    	{
    		changingColors = true;
    	}
    	else if(key == 'c' && changingColors)
    	{
    		changingColors = false;
    	}
    	else if(key == 'a' && !autoSolve)
    	{
    		autoSolve = true;
    		setup();
    	}
    	else if(key == 'a' && autoSolve)
    	{
    		autoSolve = false;
    		setup();
    	}
    }
    
    /**
     * Set file to given puzzle
     * @param puzzle 
     */
    public void setPuzzle(String puzzle)
    {
    	file = puzzle;
    	setSudokuSolver();
    	setup();
    }
    
    public void setSudokuSolver()
    {
    	unsolved = new SudokuSolver(file);
    	solved = new SudokuSolver(file);
    }
    
    public void mousePressed()
    {
    	changeColors();
    	setup();
    }
    
    /**
     * Change color theme to random
     */
    public void changeColors()
    {
    	bgR = random(200f, 200f); bgG = random(100f, 200f); bgB = random(100f, 200f);
    }
}
